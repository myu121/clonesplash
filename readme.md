# CloneSplash iOS App

This is an iOS clone application of the Unsplash photo app.

Current functionality includes:

- Getting photos by category
- View individual photo with zooming capability
- Fake login
- "Like" photos
- Download photos to photo library
- User Profile page
- Search page
- Pagination


## Getting photos by category
The home screen fetches photos by topic from the Unsplash API. The topics are selected from the bar on top. Tapping on a photo
will open that photo in a detail screen.



<img src="https://media.giphy.com/media/yV4feV8G2jSZywZ832/giphy.gif">

I have implemented pagination, meaning the photos are fetched page by page (10 photos per page). A new page of photos is fetched when the user
scrolls to the end of the table.

<img src="https://media.giphy.com/media/f1hUkwPgHXd7EsaeDi/giphy.gif">

## View individual photo with zooming capability

Tapping on any photo will open the photo screen which has zooming function.

<img src="https://media.giphy.com/media/wJuENw7cAACphKeHVV/giphy.gif">
## Fake login

The login is not a real login. Instead, it fetches the user's profile and displays it as the profile page.

<img src="https://media.giphy.com/media/rIsrY6k6gMVlBZ6pEq/giphy.gif">
## "Like" photos

Liking photos adds the photo to the user's liked photos. For this demo application, because the login is fake, the 
liked photos are not actually added to the user's account. It is only shown there for demo purposes and will reset
if the app is relaunched.

<img src="https://media.giphy.com/media/lPnioHYebEGx2PHieS/giphy.gif">
## Download photos to photo library

Tapping the download photo button will add the photo to the iPhone's photo library after asking for access.

<img src="https://media.giphy.com/media/LAVQYKL8xfPA9xhWwZ/giphy.gif">
## User Profile page

Tapping the photographer's name from the photo detail screen will go to the user's profile page.

The user page shows the user's picture, name, biography, instagram link, and portfolio link. If these are empty,
they are hidden and auto layout will adjust the constraints accordingly.
The bottom portion of the screen shows the user's photos, likes, or collections using the selector.

<img src="https://media.giphy.com/media/tMkxszYRrmZ9atRHe8/giphy.gif">
## Search page

The search page calls the search end point of the API with the user's query in the search bar.
The search is based on the selection of the segmented control. It will either search for photos, collections, or users.

<img src="https://media.giphy.com/media/rEm5VkNIGclwSlDHOv/giphy.gif"> <img src="https://media.giphy.com/media/K2I6AcZLNK0jKLSxIA/giphy.gif">
## Techniques

- JSON API fetching and parsing
- UIKit
- Collection View
- Table View
- Model-View-ViewModel (MVVM) code structure
- Delegates
- Observers
- Image downloading
- Pagination

## API Reference

https://unsplash.com/documentation

## Technologies

- iOS 13.0 or above

- Xcode 12.5

- Swift 5

## Frameworks

- UIKit

## Supported Devices

- iPhone SE (2nd gen)

- iPhone 8 - 12 (All sizes supported)