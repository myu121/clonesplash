//
//  WelcomeViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/5/21.
//

import Foundation
import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet private weak var buttonView: UIView!
    @IBOutlet private weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonView.layer.cornerRadius = 5
    }
    
    @IBAction private func buttonTapped(_ sender: Any) {
        print("button tapped")
        self.dismiss(animated: true, completion: nil)
    }
}
