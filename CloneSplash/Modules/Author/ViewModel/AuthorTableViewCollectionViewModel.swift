//
//  AuthorTableViewCollectionViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation

class AuthorTableViewCollectionViewModel {
    private var collection: Collection
    private var tags: [String] = []
    
    init(collection: Collection) {
        self.collection = collection
        if let tags = collection.tags {
            for tag in tags {
                if let type = tag.type, let title = tag.title {
                    if type == "search" {
                        self.tags.append(title)
                    }
                }
            }
        }
    }
    
    var coverPhoto: String {
        collection.coverPhoto?.urls?.regular ?? ""
    }
    
    var photo1: String {
        guard let photo1 = collection.previewPhotos?[0] else { return "" }
        return photo1.urls?.small ?? ""
    }
    
    var photo2: String {
        guard collection.previewPhotos?.count ?? 0 > 1, let photo2 = collection.previewPhotos?[1] else { return "" }
        return photo2.urls?.small ?? ""
    }
    
    var photo3: String {
        guard collection.previewPhotos?.count ?? 0 > 2, let photo3 = collection.previewPhotos?[2] else { return "" }
        return photo3.urls?.small ?? ""
    }
    
    var numPhotos: String {
        String(collection.totalPhotos ?? 0) + " photos •"
    }
    
    var curatedBy: String {
        "Curated by " + (collection.user?.name ?? "CloneSplash")
    }
    
    var tag1: String {
        guard !tags.isEmpty else { return "" }
        return tags[0]
    }
    
    var tag2: String {
        guard tags.count > 1 else { return "" }
        return tags[1]
    }
    
    var tag3: String {
        guard tags.count > 2 else { return "" }
        return tags[2]
    }
    
    var title: String {
        collection.title ?? "Untitled"
    }
}
