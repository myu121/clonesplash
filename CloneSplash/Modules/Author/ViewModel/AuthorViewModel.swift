//
//  AuthorViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/11/21.
//

import Foundation

protocol AuthorViewModelDelegate: AnyObject {
    func reloadTable()
}

class AuthorViewModel {
    private var author: User
    private let router = Router<PhotoApi>()
    private var photos: [Photo] = []
    var likedPhotos: [Photo] = []
    private var collections: [Collection] = []
    var selectedPhotos: [Photo] = [] {
        didSet {
            self.delegate?.reloadTable()
        }
    }
    weak var delegate: AuthorViewModelDelegate?
    var currentPage = 1
    var localUser = false
    
    init(author: User) {
        self.author = author
    }
    
    var instagramLink: String {
        guard let insta = author.instagramUsername else { return "" }
        return "instagram.com/\(insta)"
    }
    
    var location: String {
        author.location ?? ""
    }
    
    var name: String {
        author.name ?? ""
    }
    
    var username: String {
        author.username ?? ""
    }
    
    var portfolio: String {
        if let url = author.portfolioURL {
            if url.contains("instagram") {
                return ""
            }
            return url
        }
        return ""
    }
    
    var profileImage: String {
        author.profileImage?.medium ?? ""
    }
    
    var bio: String {
        author.bio ?? ""
    }
    
    var forHire: Bool {
        author.forHire
    }
    
    var numberOfPhotos: Int {
        selectedPhotos.count
    }
    
    func fetchPhotos(slug: String, page: Int, localUser: Bool) {
        self.localUser = localUser
        router.request(.userPhotos(id: slug, page: page)) { [weak self] (results: Result<[Photo], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                if page == 1 {
                    self.currentPage = 1
                    self.photos = []
                } else {
                    self.currentPage += 1
                }
                for photo in data {
                    self.photos.append(photo)
                }
                self.selectedPhotos = self.photos
                self.delegate?.reloadTable()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchLikedPhotos(slug: String, page: Int, localUser: Bool) {
        router.request(.userLikedPhotos(id: slug, page: page)) { [weak self] (results: Result<[Photo], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                print("fetchPage: \(page)")
                if page == 1 {
                    self.currentPage = 1
                    self.likedPhotos = []
                    if localUser {
                        self.likedPhotos.append(contentsOf: Profile.likedPhotos)
                    }
                } else {
                    self.currentPage += 1
                }
                for photo in data {
                    self.likedPhotos.append(photo)
                }
                self.selectedPhotos = self.likedPhotos
                self.delegate?.reloadTable()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchCollectionPhotos(slug: String, page: Int) {
        router.request(.userCollections(id: slug, (page: page, perPage: 3))) { [weak self] (results: Result<[Collection], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                if page == 1 {
                    self.collections = []
                } else {
                    self.currentPage += 1
                }
                for collection in data {
                    self.collections.append(collection)
                }
                self.delegate?.reloadTable()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getPhoto(at index: Int) -> Photo {
        print(index)
        return selectedPhotos[index]
    }
    
    func getLikedPhoto(at index: Int) -> Photo {
        likedPhotos[index]
    }
    
    func getCollection(at index: Int) -> Collection {
        collections[index]
    }
    
    func numberOfCollections() -> Int {
        collections.count
    }
    
    func totalLikes() -> Int {
        author.totalLikes ?? 0
    }
    
    func totalPhotos() -> Int {
        author.totalPhotos ?? 0
    }
    
    func totalCollections() -> Int {
        author.totalCollections ?? 0
    }
}
