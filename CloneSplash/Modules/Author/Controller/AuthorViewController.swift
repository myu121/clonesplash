//
//  AuthorViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/10/21.
//

import Foundation
import UIKit

protocol AuthorViewControllerDelegate: AnyObject {
    func unBlur()
}

class AuthorViewController: UIViewController {
    @IBOutlet private weak var segmentControl: UISegmentedControl! {
        didSet {
            print(segmentControl.selectedSegmentIndex)
        }
    }
    @IBOutlet private weak var profileImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var instagramButton: UIButton!
    @IBOutlet private weak var instagramView: UIView!
    @IBOutlet private weak var rootView: UIView!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var shareButton: UIButton!
    @IBOutlet private weak var portfolioButton: UIButton!
    @IBOutlet private weak var hireLabel: UILabel!
    @IBOutlet private weak var bioLabel: UILabel!
    @IBOutlet private weak var checkmarkImageView: UIImageView!
    @IBOutlet private weak var hireView: UIView!
    @IBOutlet private weak var locationView: UIView!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        }
    }

    var viewModel: AuthorViewModel?
    weak var delegate: AuthorViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        viewModel?.delegate = self
        segmentControl.addTarget(self, action: #selector(segmentControlValueChanged(_:)), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.delegate = self
        guard let username = viewModel?.username else { return }
        getPhotos(for: getSegmentControlIndex(), slug: username, page: 1)
        segmentControl.addTarget(self, action: #selector(segmentControlValueChanged(_:)), for: .valueChanged)
    }
    
    @objc
    func segmentControlValueChanged(_ sender: UISegmentedControl) {
        let index = segmentControl.selectedSegmentIndex
        guard let username = viewModel?.username else { return }
        getPhotos(for: index, slug: username, page: 1)
    }
    
    func getPhotos(for index: Int, slug: String, page: Int) {
        switch index {
        case 0:
            viewModel?.fetchPhotos(slug: slug, page: page, localUser: false)
            
        case 1:
            viewModel?.fetchLikedPhotos(slug: slug, page: page, localUser: false)
            
        case 2:
            viewModel?.fetchCollectionPhotos(slug: slug, page: page)
            
        default:
            break
        }
    }
    
    func setUpViews() {
        guard let viewModel = viewModel else { return }
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        segmentControl.selectedSegmentTintColor = UIColor.gray
        rootView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        nameLabel.text = viewModel.name
        locationLabel.text = viewModel.location
        instagramButton.setTitle(viewModel.instagramLink, for: .normal)
        profileImageView.sd_setImage(with: URL(string: viewModel.profileImage), completed: nil)
        profileImageView.layer.cornerRadius = profileImageView.frame.width / 2.0
        if viewModel.portfolio.isEmpty == true {
            portfolioButton.frame = CGRect(x: portfolioButton.frame.minX, y: portfolioButton.frame.minY, width: 0, height: 0)
        }
        portfolioButton.setTitle(viewModel.portfolio, for: .normal)
        bioLabel.text = viewModel.bio
        
        if viewModel.instagramLink.isEmpty {
            instagramView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        if !viewModel.forHire {
            hireView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        if viewModel.location.isEmpty {
            locationView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        if viewModel.portfolio.isEmpty {
            portfolioButton.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        nameLabel.isHidden = false
    }
    
    @IBAction private func closeButtonTapped(_ sender: Any) {
        self.delegate?.unBlur()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func instagramTapped(_ sender: Any) {
        guard let insta = viewModel?.instagramLink else { return }
        var instagramHooks = insta
        if let instagramUrl = NSURL(string: instagramHooks) {
            if UIApplication.shared.canOpenURL(instagramUrl as URL) {
                UIApplication.shared.openURL(instagramUrl as URL)
            } else {
                // redirect to safari because the user doesn't have Instagram
                if let safari = NSURL(string: "https://\(insta)") as URL? {
                    UIApplication.shared.openURL(safari)
                }
            }
        }
    }
    
    @IBAction private func portfolioTapped(_ sender: Any) {
        guard let portfolio = viewModel?.portfolio else { return }
        
        // redirect to safari because the user doesn't have Instagram
        if let safariURL = NSURL(string: portfolio) as URL? {
            UIApplication.shared.openURL(safariURL)
        }
    }
    
    func getSegmentControlIndex() -> Int {
        segmentControl.selectedSegmentIndex
    }
}

extension AuthorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = segmentControl.selectedSegmentIndex
        switch index {
        case 0, 1:
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "PhotoDetailController") as? PhotoDetailController,
                  let vm = viewModel else { fatalError("Unable to instantiate photo detail screen") }
            vc.viewModel = PhotoDetailViewModel(photo: vm.getPhoto(at: indexPath.row))
            vc.modalPresentationStyle = .automatic
            vc.presentingVC = self
            self.present(vc, animated: true, completion: nil)
            
        case 2:
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "CollectionsDetailController") as? CollectionsDetailController,
                  let vm = viewModel else { fatalError("Unable to instantiate collection detail screen") }
            vc.viewModel = CollectionsDetailViewModel(collection: vm.getCollection(at: indexPath.row))
            vc.modalPresentationStyle = .automatic
            self.present(vc, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

extension AuthorViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let vm = viewModel else { return 0 }
        let index = segmentControl.selectedSegmentIndex
        if index == 2 {
            return vm.numberOfCollections()
        }
        if index == 1 {
            if vm.localUser {
                return Profile.likedPhotos.count
            } else {
                return vm.numberOfPhotos
            }
        } else {
            if vm.localUser {
                return Profile.photos.count + vm.numberOfPhotos
            }
            return vm.numberOfPhotos
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = segmentControl.selectedSegmentIndex
        
        if index == 2 {
            // collections
            if indexPath.row == ((viewModel?.numberOfCollections() ?? 0) - 1) {
                // fetch more collections
                
                if let username = viewModel?.username, let page = viewModel?.currentPage, let collections = viewModel?.numberOfCollections() {
                    print("currentPage: \(page)")
                    if collections > page * 3 {
                        self.getPhotos(for: index, slug: username, page: page + 1)
                    }
                }
            }
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorTableViewCollectionCell") as? AuthorTableViewCollectionCell else {
                fatalError("could not dequeue cell")
            }
            if let collection = viewModel?.getCollection(at: indexPath.row) {
                cell.configure(configurator: AuthorTableViewCollectionViewModel(collection: collection))
            }
            cell.selectionStyle = .none
            return cell
        }
        if index == 0 {
            // photos
            // checking if last cell
            print("index row: \(indexPath.row)")
            if indexPath.row == ((viewModel?.numberOfPhotos ?? 0) - 1) {
                // fetch more photos
                
                if let username = viewModel?.username, let page = viewModel?.currentPage, let photos = viewModel?.totalPhotos() {
                    print("currentPage: \(page)")
                    if photos > page * 10 {
                        self.getPhotos(for: index, slug: username, page: page + 1)
                    }
                }
            }
        }
        
        if index == 1 {
            // likes
            // checking if last cell
            print("index row: \(indexPath.row)")
            if indexPath.row == ((viewModel?.numberOfPhotos ?? 0) - 1) {
                // fetch more photos
                
                if let username = viewModel?.username, let page = viewModel?.currentPage, let likes = viewModel?.totalLikes() {
                    print("currentPage: \(page)")
                    if likes > page * 10 {
                        self.getPhotos(for: index, slug: username, page: page + 1)
                    }
                }
            }
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell") as? PhotoTableViewCell else {
                fatalError("could not dequeue cell")
            }
            if let photo = viewModel?.getPhoto(at: indexPath.row) {
                cell.configure(photo: PhotoCellViewModel(photo: photo, image: nil))
            }
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorTableViewCell") as? AuthorTableViewCell else {
            fatalError("could not dequeue cell")
        }
        if let localUser = viewModel?.localUser {
            if localUser && index == 0 {
                if indexPath.row < Profile.photos.count {
                    cell.configure(photo: PhotoCellViewModel(photo: Photo(id: nil, urls: nil, likes: nil, user: nil), image: Profile.photos[indexPath.row]))
                    return cell
                }
            }
        }
        if let photo = viewModel?.getPhoto(at: indexPath.row) {
            cell.configure(photo: PhotoCellViewModel(photo: photo, image: nil))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = segmentControl.selectedSegmentIndex
        
        if index < 2 {
            let photo = viewModel?.getPhoto(at: indexPath.row)
            
            let width = Int(self.view.frame.width)
            let adjustedHeight = Double(photo?.height ?? 0) * (Double(width) / Double(photo?.width ?? 0))
            
            return CGFloat(adjustedHeight)
        }
        return CGFloat(400)
    }
}

extension AuthorViewController: AuthorViewModelDelegate {
    func reloadTable() {
        tableView.reloadData()
    }
}
