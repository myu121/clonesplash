//
//  AuthorTableViewCell.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/11/21.
//

import Foundation
import UIKit

class AuthorTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    
    func configure(photo: PhotoCellViewModel) {
        photoImageView.sd_imageTransition = .none
        if let image = photo.image {
            photoImageView.image = image
        } else {
            photoImageView.sd_setImage(with: URL(string: photo.url))
        }
    }
}
