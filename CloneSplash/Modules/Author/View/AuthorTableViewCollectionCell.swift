//
//  AuthorTableViewCollectionCell.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation
import UIKit

class AuthorTableViewCollectionCell: UITableViewCell {
    @IBOutlet private weak var photo1ImageView: UIImageView!
    @IBOutlet private weak var photo2ImageView: UIImageView!
    @IBOutlet private weak var photo3ImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var numPhotosLabel: UILabel!
    @IBOutlet private weak var curatedByLabel: UILabel!
    @IBOutlet private weak var tag1Label: UILabel!
    @IBOutlet private weak var tag2Label: UILabel!
    @IBOutlet private weak var tag3Label: UILabel!
    @IBOutlet private weak var tag1View: UIView!
    @IBOutlet private weak var tag2View: UIView!
    @IBOutlet private weak var tag3View: UIView!

    func configure(configurator: AuthorTableViewCollectionViewModel) {
        photo1ImageView.sd_setImage(with: URL(string: configurator.coverPhoto), completed: nil)
        photo2ImageView.sd_setImage(with: URL(string: configurator.photo2), completed: nil)
        photo3ImageView.sd_setImage(with: URL(string: configurator.photo3), completed: nil)
        tag1View.layer.cornerRadius = 5.0
        tag2View.layer.cornerRadius = 5.0
        tag3View.layer.cornerRadius = 5.0
        if configurator.tag1.isEmpty {
            tag1View.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        if configurator.tag2.isEmpty {
            tag2View.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        if configurator.tag3.isEmpty {
            tag3View.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        tag1Label.text = configurator.tag1
        tag2Label.text = configurator.tag2
        tag3Label.text = configurator.tag3
        photo1ImageView.layer.cornerRadius = 5.0
        photo2ImageView.layer.cornerRadius = 5.0
        photo3ImageView.layer.cornerRadius = 5.0
        titleLabel.text = configurator.title
        curatedByLabel.text = configurator.curatedBy
        numPhotosLabel.text = configurator.numPhotos
    }
}
