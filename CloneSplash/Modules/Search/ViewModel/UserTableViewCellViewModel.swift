//
//  UserTableViewCellViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation

class UserTableViewCellViewModel {
    private var user: User
    
    init(user: User) {
        self.user = user
    }
    
    var name: String {
        user.name ?? ""
    }
    
    var location: String {
        user.location ?? ""
    }
    
    var username: String {
        user.username ?? ""
    }
    
    var picture: String {
        user.profileImage?.medium ?? ""
    }
}
