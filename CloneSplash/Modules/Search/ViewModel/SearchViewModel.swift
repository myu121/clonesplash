//
//  SearchViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation

// MARK: - Protocol
protocol SearchViewModelDelegate: AnyObject {
    func reloadTable()
}

// MARK: - ViewModel
class SearchViewModel {
    // MARK: - Properties
    private var searchPhotos: [Photo] = []
    private var searchCollections: [Collection] = []
    private var searchUsers: [User] = []
    private let router = Router<PhotoApi>()
    weak var delegate: SearchViewModelDelegate?
    var currentPage = 1
    var lastSearch = 0
    var noResults = false
    
    // MARK: - Methods
    func fetchSearchPhoto(query: String, page: Int) {
        router.request(.searchPhotos((query: query, page: page))) { [weak self] (results: Result<PhotoSearch, AppError>) in
            guard let self = self else { return }
            switch results {
            case .success(let data):
                if page == 1 {
                    self.searchPhotos = []
                }
                if data.total == 0 {
                    self.noResults = true
                }
                for photo in data.results {
                    self.searchPhotos.append(photo)
                }
                self.lastSearch = 0
                self.currentPage = page
                self.delegate?.reloadTable()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchSearchCollection(query: String, page: Int) {
        router.request(.searchCollections((query: query, page: page))) { [weak self] (results: Result<CollectionSearch, AppError>) in
            guard let self = self else { return }
            switch results {
            case .success(let data):
                if page == 1 {
                    self.searchCollections = []
                }
                for collection in data.results {
                    self.searchCollections.append(collection)
                }
                self.lastSearch = 1
                self.currentPage = page
                self.delegate?.reloadTable()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchSearchUser(query: String, page: Int) {
        router.request(.searchUsers((query: query, page: page))) { [weak self] (results: Result<UserSearch, AppError>) in
            guard let self = self else { return }
            switch results {
            case .success(let data):
                if page == 1 {
                    self.searchUsers = []
                }
                for user in data.results {
                    self.searchUsers.append(user)
                }
                self.lastSearch = 2
                self.currentPage = page
                self.delegate?.reloadTable()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func numberOfPhotos() -> Int {
        searchPhotos.count
    }
    
    func numberOfCollections() -> Int {
        searchCollections.count
    }
    
    func numberOfUsers() -> Int {
        searchUsers.count
    }
    
    func getPhoto(at index: Int) -> Photo {
        searchPhotos[index]
    }
    
    func getCollection(at index: Int) -> Collection {
        searchCollections[index]
    }
    
    func getUser(at index: Int) -> User {
        searchUsers[index]
    }
}
