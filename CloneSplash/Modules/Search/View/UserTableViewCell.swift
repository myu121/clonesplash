//
//  UserTableViewCell.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation
import UIKit

// MARK: - Cell
class UserTableViewCell: UITableViewCell {
    // MARK: - Properties
    @IBOutlet private weak var profilePictureImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var locationImageView: UIImageView!
    
    // MARK: - Methods
    func configure(configurator: UserTableViewCellViewModel) {
        profilePictureImageView.sd_setImage(with: URL(string: configurator.picture), completed: nil)
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.frame.height / 2.0
        nameLabel.text = configurator.name
        usernameLabel.text = configurator.username
        locationLabel.text = configurator.location
        if configurator.location.isEmpty {
            locationImageView.isHidden = true
        }
    }
}
