//
//  PhotoSearch.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation

// MARK: - Classes
class PhotoSearch: Decodable {
    var total: Int
    var totalPages: Int
    var results: [Photo]
    
    enum CodingKeys: String, CodingKey {
        case total, results
        case totalPages = "total_pages"
    }
}

class CollectionSearch: Decodable {
    var total: Int
    var totalPages: Int
    var results: [Collection]
    
    enum CodingKeys: String, CodingKey {
        case total, results
        case totalPages = "total_pages"
    }
}

class UserSearch: Decodable {
    var total: Int
    var totalPages: Int
    var results: [User]
    
    enum CodingKeys: String, CodingKey {
        case total, results
        case totalPages = "total_pages"
    }
}
