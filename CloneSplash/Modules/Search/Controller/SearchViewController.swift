//
//  SearchViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/12/21.
//

import Foundation
import UIKit

class SearchViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        }
    }
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    @IBOutlet private weak var segmentControl: UISegmentedControl!
    
    lazy var viewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        viewModel.delegate = self
        segmentControl.addTarget(self, action: #selector(segmentControlValueChanged(_:)), for: .valueChanged)
    }
    
    @objc
    func segmentControlValueChanged(_ sender: UISegmentedControl) {
        let index = segmentControl.selectedSegmentIndex
        switch index {
        case 0:
            viewModel.fetchSearchPhoto(query: searchBar.text ?? "", page: 1)
            
        case 1:
            viewModel.fetchSearchCollection(query: searchBar.text ?? "", page: 1)
            
        case 2:
            viewModel.fetchSearchUser(query: searchBar.text ?? "", page: 1)
            
        default:
            break
        }
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if viewModel.lastSearch == 0 {
            // photos
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "PhotoDetailController") as? PhotoDetailController else { fatalError("Unable to instantiate photo detail screen") }
            vc.viewModel = PhotoDetailViewModel(photo: self.viewModel.getPhoto(at: indexPath.row))
            vc.modalPresentationStyle = .automatic
            self.present(vc, animated: true, completion: nil)
        }
        if viewModel.lastSearch == 2 {
            // users
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "AuthorViewController") as? AuthorViewController else { fatalError("Unable to instantiate author screen") }
            vc.modalPresentationStyle = .overCurrentContext
            vc.delegate = self
            let selectedUser = viewModel.getUser(at: indexPath.row)
            vc.viewModel = AuthorViewModel(author: selectedUser)
            
            self.present(vc, animated: true, completion: nil)
            let darkBlur = UIBlurEffect(style: .dark)
            let blurView = UIVisualEffectView(effect: darkBlur)
            blurView.frame = self.view.frame // your view that have any objects
            blurView.tag = 1
            self.view.addSubview(blurView)
        } else {
            // collections
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "CollectionsDetailController") as? CollectionsDetailController else { fatalError("Unable to instantiate collection detail screen") }
            vc.viewModel = CollectionsDetailViewModel(collection: viewModel.getCollection(at: indexPath.row))
            vc.modalPresentationStyle = .automatic
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let index = segmentControl.selectedSegmentIndex
        switch index {
        case 0:
            return viewModel.numberOfPhotos()
            
        case 1:
            return viewModel.numberOfCollections()
            
        case 2:
            return viewModel.numberOfUsers()
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = segmentControl.selectedSegmentIndex
        // checking if last cell
        if indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {
            // fetch more photos
            switch index {
            case 0:
                viewModel.fetchSearchPhoto(query: searchBar.text ?? "", page: viewModel.currentPage + 1)
                
            case 1:
                viewModel.fetchSearchCollection(query: searchBar.text ?? "", page: viewModel.currentPage + 1)
                
            case 2:
                viewModel.fetchSearchUser(query: searchBar.text ?? "", page: viewModel.currentPage + 1)
                
            default:
                break
            }
        }
        
        switch index {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell") as? PhotoTableViewCell else {
                fatalError("could not dequeue cell")
            }
            let photo = viewModel.getPhoto(at: indexPath.row)
            cell.configure(photo: PhotoCellViewModel(photo: photo, image: nil))
            cell.selectionStyle = .none
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorTableViewCollectionCell") as? AuthorTableViewCollectionCell else {
                fatalError("could not dequeue cell")
            }
            let collection = viewModel.getCollection(at: indexPath.row)
            cell.configure(configurator: AuthorTableViewCollectionViewModel(collection: collection))
            cell.selectionStyle = .none
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as? UserTableViewCell else {
                fatalError("could not dequeue cell")
            }
            let user = viewModel.getUser(at: indexPath.row)
            cell.configure(configurator: UserTableViewCellViewModel(user: user))
            cell.selectionStyle = .none
            return cell
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = segmentControl.selectedSegmentIndex
        if index == 0 {
            let photo = viewModel.getPhoto(at: indexPath.row)
            let width = Int(self.view.frame.width)
            let adjustedHeight = Double(photo.height ?? 0) * (Double(width) / Double(photo.width ?? 0))
            return CGFloat(adjustedHeight)
        }
        if index == 2 {
            return CGFloat(182)
        }
        return CGFloat(404)
    }
}

// MARK: - Extension: SearchBarDelegate

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let index = segmentControl.selectedSegmentIndex
        switch index {
        case 0:
            viewModel.fetchSearchPhoto(query: searchBar.text ?? "", page: 1)
            
        case 1:
            viewModel.fetchSearchCollection(query: searchBar.text ?? "", page: 1)
            
        case 2:
            viewModel.fetchSearchUser(query: searchBar.text ?? "", page: 1)
            
        default:
            break
        }
    }
}

extension SearchViewController: SearchViewModelDelegate {
    func reloadTable() {
        tableView.reloadData()
        if !viewModel.noResults {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
}

extension SearchViewController: AuthorViewControllerDelegate {
    func unBlur() {
        if let blurView = self.view.viewWithTag(1) {
            blurView.removeFromSuperview()
        }
    }
}
