//
//  LoginViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/13/21.
//

import Foundation
import UIKit

// MARK: - Delegate protocol
protocol LoginViewControllerDelegate: AnyObject {
    func login(username: String)
}

// MARK: - Controller
class LoginViewController: UIViewController {
    // MARK: - Properties
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var loginButtonView: UIView!
    weak var delegate: LoginViewControllerDelegate?
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        loginButtonView.layer.cornerRadius = 5.0
    }
    
    @IBAction private func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func loginButtonTapped(_ sender: Any) {
        self.delegate?.login(username: usernameTextField.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}
