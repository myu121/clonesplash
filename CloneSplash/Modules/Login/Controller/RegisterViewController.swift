//
//  RegisterViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/13/21.
//

import Foundation
import UIKit

protocol RegisterViewControllerDelegate: AnyObject {
    func register(username: String, firstName: String, lastName: String, email: String, password: String)
}

class RegisterViewController: UIViewController {
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameTextField: UITextField!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var registerButton: UIButton!
    weak var delegate: RegisterViewControllerDelegate?

    @IBAction private func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func registerButtonTapped(_ sender: Any) {
        self.delegate?.register(username: usernameTextField.text ?? "",
                                firstName: firstNameTextField.text ?? "",
                                lastName: lastNameTextField.text ?? "",
                                email: emailTextField.text ?? "",
                                password: passwordTextField.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}
