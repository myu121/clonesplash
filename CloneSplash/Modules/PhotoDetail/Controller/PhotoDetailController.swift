//
//  PhotoDetailController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/10/21.
//

import Foundation
import UIKit

class PhotoDetailController: UIViewController {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var favoriteButton: UIButton!
    @IBOutlet private weak var plusButton: UIButton!
    @IBOutlet private weak var downloadButton: UIButton!
    @IBOutlet private weak var shareButton: UIButton!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var infoButton: UIButton!
    @IBOutlet private weak var nameButton: UIButton!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var favoriteButtonView: UIView!
    @IBOutlet private weak var plusButtonView: UIView!
    @IBOutlet private weak var downloadButtonView: UIView!

    var viewModel: PhotoDetailViewModel?
    var presentingVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let urlString = viewModel?.photoURL {
            imageView.sd_setImage(with: URL(string: urlString), completed: nil)
        }
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        scrollView.delegate = self
        if let name = viewModel?.name {
            nameButton.setTitle(name, for: .normal)
        }
        favoriteButtonView.layer.cornerRadius = favoriteButtonView.frame.height / 2.0
        plusButtonView.layer.cornerRadius = plusButtonView.frame.height / 2.0
        downloadButtonView.layer.cornerRadius = downloadButtonView.frame.height / 2.0
        
        // check if user liked
        for likedPhoto in Profile.likedPhotos {
            if let id = viewModel?.id {
                if likedPhoto.id == id {
                    favoriteButton.isSelected = true
                }
            }
        }
    }
    
    @IBAction private func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func downloadButtonTapped(_ sender: Any) {
        if let image = imageView.image {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
    
    @IBAction private func heartButtonTapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        favoriteButton.isSelected.toggle()
        if !Profile.loggedIn {
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { fatalError("Unable to instantiate login screen") }
            vc.modalPresentationStyle = .automatic
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        } else {
            if !Profile.likedPhotos.contains(where: { photo in
                photo.id == vm.id
            }) {
                // Profile.likedPhotos.append(vm.getPhoto())
                Profile.likedPhotos.insert(vm.getPhoto(), at: 0)
            } else {
                Profile.likedPhotos.removeAll { photo in
                    photo.id == vm.id ?? ""
                }
                if let pvc = presentingVC as? ProfileViewController {
                    pvc.viewModel?.delegate?.reloadTable()
                }
            }
        }
    }
    
    @IBAction private func authorTapped(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "AuthorViewController") as? AuthorViewController else { fatalError("Unable to instantiate author screen") }
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        if let author = viewModel?.user {
            vc.viewModel = AuthorViewModel(author: author)
        }
        self.present(vc, animated: true, completion: nil)
        let darkBlur = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.frame // your view that have any objects
        blurView.tag = 1
        self.view.addSubview(blurView)
    }
}

extension PhotoDetailController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        imageView
    }
}

extension PhotoDetailController: AuthorViewControllerDelegate {
    func unBlur() {
        if let blurView = self.view.viewWithTag(1) {
            blurView.removeFromSuperview()
        }
    }
}

extension PhotoDetailController: LoginViewControllerDelegate {
    func login(username: String) {
        Profile.fetchUser(username: username) {
            if let vm = self.viewModel {
                Profile.likedPhotos.append(vm.getPhoto())
            }
        }
        Profile.fetchLikedPhotos(slug: username)
    }
}
