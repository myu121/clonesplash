//
//  PhotoDetailViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/10/21.
//

import Foundation

class PhotoDetailViewModel {
    private var photo: Photo
    
    init(photo: Photo) {
        self.photo = photo
    }
    
    var photoURL: String? {
        photo.urls?.regular
    }
    
    var user: User? {
        photo.user
    }
    
    var id: String? {
        photo.id
    }
    
    var name: String {
        let firstName = photo.user?.firstName ?? ""
        let lastName = photo.user?.lastName ?? ""
        return firstName + " " + lastName
    }
    
    func getPhoto() -> Photo {
        photo
    }
}
