//
//  PhotoTableViewCell.swift
//  ImageDownloader
//
//  Created by Michael Yu on 5/3/21.
//

import SDWebImage
import UIKit

class PhotoTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var authorLabel: UILabel!
    
    func configure(photo: PhotoCellViewModel) {
        self.photoImageView.sd_setImage(with: URL(string: photo.url))
        let firstName = photo.user?.firstName ?? ""
        let lastName = photo.user?.lastName ?? ""
        authorLabel.text = firstName + " " + lastName
    }
}
