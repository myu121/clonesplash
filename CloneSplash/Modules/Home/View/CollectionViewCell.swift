//
//  CollectionViewCell.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/7/21.
//

import Foundation
import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet private var collectionLabel: UILabel!
    @IBOutlet private var selectedView: UIView!
    
    func configure(configurator: Topic?, selected: Bool) {
        if selected {
            selectedView.layer.backgroundColor = UIColor.white.cgColor
        } else {
            selectedView.layer.backgroundColor = UIColor.darkGray.cgColor
        }
        if let configurator = configurator, let title = configurator.title {
            collectionLabel.text = title
        }
    }
}
