//
//  HomeViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/7/21.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        }
    }
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.reloadData()
        }
    }
    lazy var viewModel = HomeViewModel(delegate: self)
    weak var appDelegate: AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        // viewModel.fetchData(testing: true)
        viewModel.fetchTopics()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let appDelegate = self.appDelegate else { return }
        if !appDelegate.hasAlreadyLaunched {

            // set hasAlreadyLaunched to false
            appDelegate.setHasAlreadyLaunched()

            // display welcome screen
            displayWelcomeScreen()
        }
        collectionView.reloadData()
    }
    
    func displayWelcomeScreen() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as? WelcomeViewController else { fatalError("Unable to instantiate welcome screen") }
        vc.modalPresentationStyle = .automatic
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction private func logoTapped(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "InfoViewController") as? InfoViewController else { fatalError("Unable to instantiate photo detail screen") }
        vc.modalPresentationStyle = .automatic
        self.present(vc, animated: true, completion: nil)
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PhotoDetailController") as? PhotoDetailController else { fatalError("Unable to instantiate photo detail screen") }
        vc.viewModel = PhotoDetailViewModel(photo: self.viewModel.getPhoto(at: indexPath.row))
        vc.modalPresentationStyle = .automatic
        self.present(vc, animated: true, completion: nil)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // checking if last cell
        if indexPath.row == viewModel.numberOfPhotos() - 1 {
            // fetch more photos
            if let topic = viewModel.getCurrentTopic(), let slug = topic.slug {
                viewModel.fetchMorePhotos(slug: slug, page: viewModel.currentPage + 1)
                viewModel.currentPage += 1
            }
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell") as? PhotoTableViewCell else {
            fatalError("could not dequeue cell")
        }
        let topicPhoto = viewModel.getPhoto(at: indexPath.row)
        cell.configure(photo: PhotoCellViewModel(photo: topicPhoto, image: nil))

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfPhotos()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let topicPhoto = viewModel.getPhoto(at: indexPath.row)
        
        let width = Int(self.view.frame.width)
        let adjustedHeight = Double(topicPhoto.height ?? 0) * (Double(width) / Double(topicPhoto.width ?? 0))
        
        return CGFloat(adjustedHeight)
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func reloadTable() {
        self.tableView.reloadData()
        self.tableView.isHidden = false
    }
    
    func reloadCollection() {
        self.collectionView.reloadData()
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        tableView.isHidden = true
        let topic = viewModel.getTopic(at: indexPath.row)
        viewModel.setCurrentTopic(topic: topic)
        if let slug = topic.slug {
            viewModel.setCurrentTopicPhotos(slug: slug, page: 1)
        }
        
        for cell in collectionView.visibleCells {
            if let downcastCell = cell as? CollectionViewCell {
                downcastCell.configure(configurator: nil, selected: false)
            }
        }
    
        if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
            cell.configure(configurator: topic, selected: true)
        }
          
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfTopics()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell else { fatalError("Unable to dequeue cell") }
        
        cell.configure(configurator: viewModel.getTopic(at: indexPath.row), selected: false)
        
        if indexPath.row == 0 {
            cell.configure(configurator: nil, selected: true)
        }
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        tableView.beginUpdates()
        tableView.endUpdates()
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: 10, height: 10)
    }
}
