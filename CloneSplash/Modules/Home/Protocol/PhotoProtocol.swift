//
//  PhotoProtocol.swift
//  PhotoDownloader
//
//  Created by Michael Yu on 5/3/21.
//

import Foundation
import UIKit

protocol PhotoProtocol {
 //   var name: String { get }
    var url: String { get }
//    var state: Photo.State { get set }
//    var image: UIImage? { get set }
}
