//
//  CollectionCellViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/7/21.
//

import Foundation

class CollectionCellViewModel {
    let collection: Collection
    
    init(collection: Collection) {
        self.collection = collection
    }
    
    var title: String {
        collection.title ?? ""
    }
    
    var photos: [Photo] {
        var allPhotos: [Photo] = []
        if let tags = collection.tags {
            for tag in tags {
                if let src = tag.source, let photo = src.coverPhoto {
                    allPhotos.append(photo)
                }
            }
        }
//        if let coverPhoto = collection.coverPhoto {
//            allPhotos.append(coverPhoto)
//        }
        if let previewPhotos = collection.previewPhotos {
            for photo in previewPhotos {
                allPhotos.append(photo)
            }
        }
        return allPhotos
    }
}
