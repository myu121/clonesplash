//
//  HomeViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/7/21.
//

import Foundation

protocol HomeViewModelDelegate: AnyObject {
    func reloadTable()
    func reloadCollection()
}

class HomeViewModel {
    private var collectionsDataSource: [CollectionCellViewModel] {
        didSet {
            self.delegate?.reloadCollection()
        }
    }
    private var topicsDataSource: [Topic] = [] {
        didSet {
            self.delegate?.reloadCollection()
        }
    }
    weak var delegate: HomeViewModelDelegate?
    private let router = Router<PhotoApi>()
    private var currentCollection: CollectionCellViewModel?
    private var currentTopicPhotos: [Photo] = []
    private var currentTopic: Topic?
    var currentPage: Int = 1
    
    init(delegate: HomeViewModelDelegate) {
        self.delegate = delegate
        self.collectionsDataSource = []
    }
    
    func fetchTopics() {
        router.request(.topics) { [weak self] (results: Result<[Topic], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                for topic in data {
                    self.topicsDataSource.append(topic)
                }
                if let slug = self.topicsDataSource[0].slug {
                    self.setCurrentTopicPhotos(slug: slug, page: 1)
                }
                self.currentTopic = self.topicsDataSource[0]
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setCurrentTopicPhotos(slug: String, page: Int) {
        router.request(.topicsPhotos(id: slug, page: page)) { [weak self] (results: Result<[Photo], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                self.currentTopicPhotos = []
                for photo in data {
                    self.currentTopicPhotos.append(photo)
                    self.delegate?.reloadTable()
                }
                self.delegate?.reloadTable()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchMorePhotos(slug: String, page: Int) {
        router.request(.topicsPhotos(id: slug, page: page)) { [weak self] (results: Result<[Photo], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                for photo in data {
                    self.currentTopicPhotos.append(photo)
                    self.delegate?.reloadTable()
                }
                self.delegate?.reloadTable()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchData(testing: Bool) {
        if testing {
            populateDataFromJson()
        } else {
            router.request(.collections) { [weak self] (results: Result<[Collection], AppError>) in
                guard let self = self else { return }
                
                switch results {
                case .success(let data):
                    for collection in data {
                        self.addToCollectionDataSource(collection: collection)
                        if let tags = collection.tags {
//                            tags.forEach { tag in
//                               // self.addToPhotoDataSource(tag: tag)
//                            }
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    private func populateDataFromJson() {
        if let path = Bundle.main.path(forResource: "collections", ofType: "json") {
            do {
                let dataJson = try Data(contentsOf: URL(fileURLWithPath: path))
                let data = try JSONDecoder().decode([Collection].self, from: dataJson)
               
                for collection in data {
                    self.addToCollectionDataSource(collection: collection)
                    if let tags = collection.tags {
//                        tags.forEach { tag in
//                           // self.addToPhotoDataSource(tag: tag)
//                        }
                    }
                }
                self.currentCollection = collectionsDataSource[0]
            } catch {
                print(error)
                print("No json data found")
            }
        }
    }
    
    private func addToCollectionDataSource(collection: Collection) {
        self.collectionsDataSource.append(CollectionCellViewModel(collection: collection))
    }
    
//    private func addToPhotoDataSource(tag: Tag) {
//        if let source = tag.source, let coverPhoto = tag.source?.coverPhoto {
//            let photo = PhotoCellViewModel(photo: Source(title: source.title ?? "", subtitle: source.subtitle ?? "", description: source.description ?? "", coverPhoto: coverPhoto))
//            self.photosDataSource.append(photo)
//        }
//    }
    
    func numberOfCollections() -> Int {
        self.collectionsDataSource.count
    }
    
    func numberOfPhotos() -> Int {
        currentTopicPhotos.count
    }
    
    func numberOfTopics() -> Int {
        topicsDataSource.count
        // currentCollection?.photos.count ?? 0
    }
    
//    func getPhoto(at index: Int) -> PhotoCellViewModel {
//        self.photosDataSource[index]
//    }
    
    func getCollection(at index: Int) -> CollectionCellViewModel {
        self.collectionsDataSource[index]
    }
    
    func getCurrentCollection() -> CollectionCellViewModel? {
        currentCollection
    }
    
    func getTopic(at index: Int) -> Topic {
        topicsDataSource[index]
    }
    
    func getPhoto(at index: Int) -> Photo {
        currentTopicPhotos[index]
    }
    
    func getCurrentTopic() -> Topic? {
        currentTopic
    }
    
    func setCurrentTopic(topic: Topic) {
        currentTopic = topic
    }
}
