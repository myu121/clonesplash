//
//  PhotoCellViewModel.swift
//  PhotoDownloader
//
//  Created by Michael Yu on 5/3/21.
//

import Foundation
import SDWebImage
import UIKit

class PhotoCellViewModel: PhotoProtocol {
    let photo: Photo
    var image: UIImage?

    init(photo: Photo, image: UIImage?) {
        self.photo = photo
        self.image = image
    }
    
    var user: User? {
        photo.user
    }
    
    var url: String {
        photo.urls?.small ?? ""
    }
    
    var height: Int {
        photo.height ?? 0
    }
    
    var width: Int {
        photo.width ?? 0
    }
}
