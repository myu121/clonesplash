//
//  Collection.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/7/21.
//

import Foundation
import UIKit

class Topic: Decodable {
    var id: String?
    var slug: String?
    var title: String?
    var description: String?
}

class Collection: Decodable {
    var id: String?
    var title: String?
    var featured: Bool
    var totalPhotos: Int?
    var tags: [Tag]?
    var coverPhoto: Photo?
    var previewPhotos: [Photo]?
    var user: User?
    
    init(id: String?,
         title: String?,
         featured: Bool,
         totalPhotos: Int?,
         tags: [Tag]?,
         coverPhoto: Photo?,
         previewPhotos: [Photo]?,
         user: User?) {
        self.id = id
        self.title = title
        self.featured = featured
        self.totalPhotos = totalPhotos
        self.tags = tags
        self.coverPhoto = coverPhoto
        self.previewPhotos = previewPhotos
        self.user = user
    }
    
    enum CodingKeys: String, CodingKey {
        case id, title, featured, tags, user
        case totalPhotos = "total_photos"
        case coverPhoto = "cover_photo"
        case previewPhotos = "preview_photos"
    }
}

class Tag: Decodable {
    var source: Source?
    var type: String?
    var title: String?
}

class Source: Decodable {
    var title: String?
    var subtitle: String?
    var description: String?
    var coverPhoto: Photo?
    
    init(title: String, subtitle: String, description: String, coverPhoto: Photo) {
        self.title = title
        self.subtitle = subtitle
        self.description = description
        self.coverPhoto = coverPhoto
    }
    
    enum CodingKeys: String, CodingKey {
        case title, subtitle, description
        case coverPhoto = "cover_photo"
    }
}

class Photo: Decodable {
    var id: String?
    var urls: URLs?
    var likes: Int?
    var user: User?
    var height: Int?
    var width: Int?
    
    init(id: String?, urls: URLs?, likes: Int?, user: User?) {
        self.id = id
        self.urls = urls
        self.likes = likes
        self.user = user
    }
}

class URLs: Decodable {
    var raw: String?
    var full: String?
    var regular: String?
    var small: String?
    var thumb: String?
}

class User: Decodable {
    var id: String?
    var username: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var portfolioURL: String?
    var bio: String?
    var location: String?
    var profileImage: ProfileImage?
    var instagramUsername: String?
    var totalLikes: Int?
    var totalPhotos: Int?
    var totalCollections: Int?
    var forHire = false
    
    init(id: String?,
         username: String?,
         name: String?,
         firstName: String?,
         lastName: String?,
         portfolioURL: String?,
         bio: String?,
         location: String?,
         profileImage: ProfileImage?,
         instagramUsername: String?,
         totalLikes: Int?,
         totalPhotos: Int?,
         forHire: Bool) {
        self.id = id
        self.name = name
        self.firstName = firstName
        self.lastName = lastName
        self.portfolioURL = portfolioURL
        self.bio = bio
        self.location = location
        self.profileImage = profileImage
        self.instagramUsername = instagramUsername
        self.totalLikes = totalLikes
        self.totalPhotos = totalPhotos
        self.forHire = forHire
    }
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, bio, location
        case firstName = "first_name"
        case lastName = "last_name"
        case portfolioURL = "portfolio_url"
        case profileImage = "profile_image"
        case instagramUsername = "instagram_username"
        case totalLikes = "total_likes"
        case totalPhotos = "total_photos"
        case totalCollections = "total_collections"
        case forHire = "for_hire"
    }
}

class ProfileImage: Decodable {
    var small: String?
    var medium: String?
    var large: String?
}
