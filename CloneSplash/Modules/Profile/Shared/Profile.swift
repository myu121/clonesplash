//
//  Profile.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/13/21.
//

import Foundation
import UIKit

// MARK: - Enum
enum Profile {
    // MARK: - Static Properties
    static let router = Router<PhotoApi>()
    static var likedPhotos: [Photo] = []
    static var photos: [UIImage] = []
    static var loggedIn = false
    static var user: User?
    
    // MARK: - Static Methods
    static func fetchUser(username: String, completion: @escaping () -> Void) {
        Profile.router.request(.getUser(username: username)) { (results: Result<User, AppError>) in
            switch results {
            case .success(let data):
                Profile.user = data
                Profile.loggedIn = true
                completion()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func fetchLikedPhotos(slug: String) {
        router.request(.getAllUserLikedPhotos(id: slug)) { (results: Result<[Photo], AppError>) in
            switch results {
            case .success(let data):
                for photo in data {
                    Profile.likedPhotos.append(photo)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
