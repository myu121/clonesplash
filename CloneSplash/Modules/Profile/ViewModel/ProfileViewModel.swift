//
//  ProfileViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/13/21.
//

import Foundation

// MARK: - Delegate Protocol
protocol ProfileViewModelDelegate: AnyObject {
    func userFetched()
}

// MARK: - ViewModel
class ProfileViewModel {
    // MARK: - Properties
    static var router = Router<PhotoApi>()
    weak var delegate: ProfileViewModelDelegate?
    
    // MARK: - Methods
    func getLoggedInStatus() -> Bool {
        Profile.loggedIn
    }
    
    func createProfile(user: User) {
        Profile.user = user
    }
    
    func getUser() -> User? {
        Profile.user
    }
    
    func fetchUser(username: String) {
        Profile.fetchUser(username: username) {
            if Profile.loggedIn {
                self.delegate?.userFetched()
            }
        }
    }
}
