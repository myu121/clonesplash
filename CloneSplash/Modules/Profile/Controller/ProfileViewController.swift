//
//  ProfileViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/13/21.
//

import Foundation
import UIKit

// MARK: - Controller
class ProfileViewController: AuthorViewController {
    // MARK: - Properties
    lazy var profileViewModel = ProfileViewModel()
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        profileViewModel.delegate = self
        if profileViewModel.getLoggedInStatus() == false {
            presentLoginScreen()
        } else {
            if let user = Profile.user {
                viewModel = AuthorViewModel(author: user)
            }
        }
        super.reloadTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getInitialTable()
        super.reloadTable()
        super.setUpViews()
    }
    
    // MARK: - Private Methods
    private func getInitialTable() {
        let index = getSegmentControlIndex()
        guard let username = Profile.user?.username else { return }
        getPhotos(for: index, slug: username, page: 1)
    }
    
    private func presentLoginScreen() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { fatalError("Unable to instantiate login screen") }
        vc.modalPresentationStyle = .currentContext
        vc.delegate = self
        self.tabBarController?.present(vc, animated: false, completion: nil)
    }
    
    override func getPhotos(for index: Int, slug: String, page: Int) {
        switch index {
        case 0:
            viewModel?.fetchPhotos(slug: slug, page: page, localUser: true)
            
        case 1:
            viewModel?.selectedPhotos = Profile.likedPhotos
            super.reloadTable()
            // liked photos already fetched
        case 2:
            viewModel?.fetchCollectionPhotos(slug: slug, page: page)
            
        default:
            break
        }
    }
}

// MARK: - Extension: LoginViewControllerDelegate
extension ProfileViewController: LoginViewControllerDelegate {
    func login(username: String) {
        profileViewModel.fetchUser(username: username)
    }
}

// MARK: - Extension: RegisterViewControllerDelegate
extension ProfileViewController: RegisterViewControllerDelegate {
    func register(username: String, firstName: String, lastName: String, email: String, password: String) {
        profileViewModel.createProfile(user: User(id: nil, username: username, name: firstName + " " + lastName, firstName: firstName, lastName: lastName, portfolioURL: nil, bio: nil, location: nil, profileImage: nil, instagramUsername: nil, totalLikes: 0, totalPhotos: 0, forHire: false))
    }
}

// MARK: - ProfileViewModelDelegate
extension ProfileViewController: ProfileViewModelDelegate {
    func userFetched() {
        // get liked photos
        if let username = Profile.user?.username {
            Profile.fetchLikedPhotos(slug: username)
        }
        
        if let author = profileViewModel.getUser() {
            viewModel = AuthorViewModel(author: author)
            super.viewDidLoad()
        }
    }
}

// MARK: - Extension: AuthorViewControllerDelegate
extension ProfileViewController: AuthorViewControllerDelegate {
    func unBlur() {
    }
}
