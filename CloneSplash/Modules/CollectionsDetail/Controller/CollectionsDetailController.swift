//
//  CollectionsDetailController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/14/21.
//

import Foundation
import UIKit

// MARK: - Controller
class CollectionsDetailController: UIViewController {
    // MARK: - Properties
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    var viewModel: CollectionsDetailViewModel?
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = viewModel?.collectionName
        authorLabel.text = viewModel?.author
        viewModel?.delegate = self
    }
    
    @IBAction private func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Extension: UITableViewDelegate
extension CollectionsDetailController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PhotoDetailController") as? PhotoDetailController,
              let vm = viewModel else { fatalError("Unable to instantiate photo detail screen") }
        vc.viewModel = PhotoDetailViewModel(photo: vm.getPhoto(at: indexPath.row).photo)
        vc.modalPresentationStyle = .automatic
        self.present(vc, animated: true, completion: nil)
    }
}

// MARK: - Extension: UITableViewDataSource
extension CollectionsDetailController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = viewModel?.numberOfPhotos() ?? 0
        print(rows)
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // checking if last cell
        guard let vm = viewModel else { fatalError("no viewmodel") }
        if indexPath.row == vm.numberOfPhotos() - 1 {
            // fetch more photos
            vm.fetchPhotos(id: vm.id, page: vm.currentPage + 1)
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell") as? PhotoTableViewCell else {
            fatalError("could not dequeue cell")
        }
        let photo = vm.getPhoto(at: indexPath.row)
        cell.configure(photo: photo)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let photo = viewModel?.getPhoto(at: indexPath.row)
        
        let width = Int(self.view.frame.width)
        let adjustedHeight = Double(photo?.height ?? 0) * (Double(width) / Double(photo?.width ?? 0))
        
        return CGFloat(adjustedHeight)
    }
}

// MARK: - Extension: CollectionsDetailViewModelDelegate
extension CollectionsDetailController: CollectionsDetailViewModelDelegate {
    func reloadTable() {
        tableView.reloadData()
    }
}
