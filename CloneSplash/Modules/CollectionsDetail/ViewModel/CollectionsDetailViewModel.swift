//
//  CollectionsDetailViewModel.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/14/21.
//

import Foundation

// MARK: - Delegate protocol
protocol CollectionsDetailViewModelDelegate: AnyObject {
    func reloadTable()
}

// MARK: - ViewModel
class CollectionsDetailViewModel {
    // MARK: - Properties
    private var collection: Collection
    private var photos: [PhotoCellViewModel]
    private let router = Router<PhotoApi>()
    weak var delegate: CollectionsDetailViewModelDelegate?
    var currentPage = 1
    
    var id: String {
        collection.id ?? ""
    }
    
    var collectionName: String {
        collection.title ?? ""
    }
    
    var author: String {
        "Curated by " + (collection.user?.name ?? "Anonymous")
    }
    
    // MARK: - Init
    init(collection: Collection) {
        self.collection = collection
        self.photos = []
        fetchPhotos(id: collection.id ?? "", page: 1)
    }
    
    // MARK: - Methods
    func fetchPhotos(id: String, page: Int) {
        router.request(.getCollectionPhotos(id: id, page: page)) { [weak self] (results: Result<[Photo], AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                if page == 1 {
                    self.photos = []
                }
                self.currentPage = page
                for photo in data {
                    self.photos.append(PhotoCellViewModel(photo: photo, image: nil))
                }
                self.delegate?.reloadTable()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func numberOfPhotos() -> Int {
        photos.count
    }
    
    func getPhoto(at index: Int) -> PhotoCellViewModel {
        photos[index]
    }
}
