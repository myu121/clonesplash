//
//  AddViewController.swift
//  CloneSplash
//
//  Created by Michael Yu on 5/13/21.
//

import Foundation
import UIKit

// MARK: - Controller
class AddViewController: UIViewController, ImagePickerDelegate {
    // MARK: - Methods
    func didSelect(image: UIImage?) {
        guard let image = image else { return }
        Profile.photos.append(image)
    }
    
    func didCancel() {
    }
    
    @IBOutlet private weak var addButton: UIButton!
    weak var delegate: ImagePickerDelegate?
    private var imagePicker: ImagePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker = ImagePicker(presentingController: self, withDelegate: self)
    }
    
    @IBAction private func addButtonTapped(_ sender: Any) {
        imagePicker?.showOptions()
    }
}
