//
//  PhotoApi.swift
//  ImageDownloader
//
//  Created by Michael Yu on 5/3/21.
//

import Foundation

let key1 = "y8ZyLEUer_eueQ_WcFlzk-wFRYgk6gUHGAVZaasbeG0"
let key2 = "YEC02CYe9-eoETIeZpsMqZHvBpDWimVCjSOX3qFZOAM"
let key3 = "mV5-mkyW2VJxsXlwRKG1FOfwPcQqOXgiS6_MNuktuIA"
var currentKey = key1

enum PhotoApi {
    case collections
    case topics
    case topicsPhotos(id: String, page: Int)
    case userPhotos(id: String, page: Int)
    case userLikedPhotos(id: String, page: Int)
    case userCollections(id: String, (page: Int, perPage: Int))
    case searchPhotos((query: String, page: Int))
    case searchCollections((query: String, page: Int))
    case searchUsers((query: String, page: Int))
    case getUser(username: String)
    case getCollectionPhotos(id: String, page: Int)
    case getAllUserLikedPhotos(id: String)
}

extension PhotoApi: EndPointType {
    var path: String {
        switch self {
        case .collections:
            return "collections/"
        case .topics:
            return "topics/"
        case .topicsPhotos(let id, _):
            return "topics/\(id)/photos"
        case .userPhotos(let id, _):
            return "users/\(id)/photos"
        case .userLikedPhotos(let id, _):
            return "users/\(id)/likes"
        case .userCollections(let id, (_, _)):
            return "users/\(id)/collections"
        case .searchPhotos((_, _)):
            return "search/photos"
        case .searchCollections((_, _)):
            return "search/collections"
        case .searchUsers((_, _)):
            return "search/users"
        case .getUser(let username):
            return "users/\(username)"
        case .getCollectionPhotos(let id, _):
            return "collections/\(id)/photos"
        case .getAllUserLikedPhotos(let id):
            return "users/\(id)/likes"
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .collections, .topics, .getUser(_), .getAllUserLikedPhotos(_):
            return .requestParameters((bodyParameters: nil,
                                       urlParameters: ["client_id": currentKey]))
            
        case .topicsPhotos(_, let page), .userPhotos(_, let page), .userLikedPhotos(_, let page), .getCollectionPhotos(_, let page):
            return .requestParameters((bodyParameters: nil,
                                       urlParameters: ["page": page, "client_id": currentKey]))
            
        case .userCollections(_, let (page, perPage)):
            return .requestParameters((bodyParameters: nil,
                                       urlParameters: ["page": page, "per_page": perPage, "client_id": currentKey]))
            
        case .searchPhotos(let (query, page)), .searchCollections(let (query, page)), .searchUsers(let (query, page)):
            return .requestParameters((bodyParameters: nil, urlParameters: ["page": page, "query": query, "client_id": currentKey]))
        }
    }
}
