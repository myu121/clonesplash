//
//  ImagePicker.swift
//  MediaAccess
//
//  Created by Michael Yu on 4/13/21.
//

import Foundation
import MobileCoreServices
import Photos
import PhotosUI
import UIKit

protocol ImagePickerDelegate: AnyObject {
    func didSelect(image: UIImage?)
    func didCancel()
}

class ImagePicker: NSObject {
    private weak var delegate: ImagePickerDelegate?
    private weak var controller: UIViewController?
    
    private let imagePicker: UIImagePickerController
    
    init(presentingController controller: UIViewController, withDelegate delegate: ImagePickerDelegate?) {
        self.delegate = delegate
        self.controller = controller
        self.imagePicker = UIImagePickerController()
        super.init()
        
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        self.imagePicker.mediaTypes = ["public.image", "public.movie"]
    }
    
    func showOptions() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if let cameraAction = self.getAction(type: .camera, title: "Take Photo") {
            alert.addAction(cameraAction)
        }
        if let libraryAction = self.getAction(type: .photoLibrary, title: "Photo Library") {
            alert.addAction(libraryAction)
        }
        if let rollAction = self.getAction(type: .savedPhotosAlbum, title: "Camera Roll") {
            alert.addAction(rollAction)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.controller?.present(alert, animated: true, completion: nil)
    }
    
    private func getAction(type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        let action = UIAlertAction(title: title, style: .default) { [weak self] _ in
            guard let self = self else { return }
            switch type {
            case .photoLibrary:
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                
                case .notDetermined:
                    self.requestLibraryAccess()
                    
                case .restricted, .denied:
                    self.displayLibraryAccessAlert()
                    
                case .authorized:
                    self.presentLibrary()
                    
                case .limited:
                    self.presentLimitedLibrary()
                    
                @unknown default:
                    break
                }
                
            case .camera:
                let status = AVCaptureDevice.authorizationStatus(for: .video)
                switch status {
                case .notDetermined:
                    self.requestCameraAccess()
                    
                case .restricted, .denied:
                    self.displayCameraAccessAlert()
                    
                case .authorized:
                    self.presentCamera()
                    
                @unknown default:
                    break
                }
                
            case .savedPhotosAlbum:
                
                break
            @unknown default:
                break
            }
        }
        return action
    }
    
    private func displayCameraAccessAlert() {
        self.controller?.showAlert(title: "Alert", message: "Allow camera access from settings", buttons: [.ok]) { _, _ in
        }
    }
    
    private func requestCameraAccess() {
        AVCaptureDevice.requestAccess(for: .video) { status in
            guard status else { return }
            self.presentCamera()
        }
    }
    
    private func presentCamera() {
        self.imagePicker.sourceType = .camera
        self.controller?.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func picker(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        self.delegate?.didSelect(image: image)
    }
    
    private func requestLibraryAccess() {
        PHPhotoLibrary.requestAuthorization { status in
            if status == .authorized {
                self.presentLibrary()
            }
        }
    }
    
    private func presentLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.controller?.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func displayLibraryAccessAlert() {
        self.controller?.showAlert(title: "Alert", message: "Allow photo library access from settings", buttons: [.ok]) { _, _ in
        }
    }
    
    private func presentLimitedLibrary() {
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return self.picker(picker, didSelect: nil)
        }
        self.picker(picker, didSelect: image)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.delegate?.didCancel()
    }
}

extension ImagePicker: UINavigationControllerDelegate { }
