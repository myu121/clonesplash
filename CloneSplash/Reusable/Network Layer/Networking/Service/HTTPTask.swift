//
//  HTTPTask.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case request
    case requestParameters((bodyParameters: Parameters?, urlParameters: Parameters?))
    case requestParametersAndHeaders((bodyParameters: Parameters?,
                                     urlParameters: Parameters?,
                                     additionHeaders: HTTPHeaders?))
}
